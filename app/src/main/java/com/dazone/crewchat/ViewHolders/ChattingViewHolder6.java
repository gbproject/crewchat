package com.dazone.crewchat.ViewHolders;

import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.dazone.crewchat.R;
import com.dazone.crewchat.database.AllUserDBHelper;
import com.dazone.crewchat.dto.ChattingDto;
import com.dazone.crewchat.dto.MsgDto;
import com.dazone.crewchat.dto.TreeUserDTOTemp;
import com.dazone.crewchat.fragment.CompanyFragment;
import com.dazone.crewchat.utils.Constant;
import com.dazone.crewchat.utils.CrewChatApplication;
import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by maidinh on 25/1/2017.
 */

public class ChattingViewHolder6 extends BaseChattingHolder {
    private TextView tv_6;
    String TAG="ChattingViewHolder6";
    public ChattingViewHolder6(View v) {
        super(v);
    }

    @Override
    protected void setup(View v) {
        tv_6 = (TextView) v.findViewById(R.id.tv_6);
    }

    @Override
    public void bindData(final ChattingDto dto) {

        String listUser = "";
        String s = dto.getMessage().trim();
        if (s.startsWith("{") && dto.getType() == 6) {

            List<TreeUserDTOTemp> allUser = null;
            if (CompanyFragment.instance != null) allUser = CompanyFragment.instance.getUser();
            if (allUser == null) allUser = new ArrayList<>();

            MsgDto msgDto = new Gson().fromJson(s, MsgDto.class);

            List<Integer> lst = msgDto.getData().getUserNos();
            for (int i = 0; i < lst.size(); i++) {
                int a = lst.get(i);
                if (i == lst.size() - 1) {
                    listUser += Constant.getUserName(allUser, a);
                } else {
                    listUser += Constant.getUserName(allUser, a) + ", ";
                }
            }
            String msg = "" + Constant.getUserName(allUser, msgDto.getData().getSubjectUserNo()) + " "
                    + CrewChatApplication.getInstance().getResources().getString(R.string.cancel_invite) + " "
                    + listUser + " "
                    + CrewChatApplication.getInstance().getResources().getString(R.string.cancel_invite_2);
            tv_6.setText(msg);
        } else {
            tv_6.setText(s);
        }

    }
}