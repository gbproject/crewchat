package com.dazone.crewchat.ViewHolders;

import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dazone.crewchat.dto.ChattingDto;
import com.dazone.crewchat.R;
import com.google.gson.Gson;

public class ChattingGroupViewHolderNew extends BaseChattingHolder {
    private TextView group_name;
    private ProgressBar progressBar;
    String TAG = "ChattingGroupViewHolderNew";

    public ChattingGroupViewHolderNew(View v) {
        super(v);
    }

    @Override
    protected void setup(View v) {
        group_name = (TextView) v.findViewById(R.id.group_name);
        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
    }

    @Override
    public void bindData(ChattingDto dto) {
        progressBar.setVisibility(View.GONE);
        group_name.setText(dto.getMessage());

    }
}