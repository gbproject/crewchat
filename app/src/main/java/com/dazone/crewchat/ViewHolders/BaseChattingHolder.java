package com.dazone.crewchat.ViewHolders;

import android.view.View;

import com.dazone.crewchat.dto.ChattingDto;

public abstract class BaseChattingHolder extends ItemViewHolder<ChattingDto> {
    public BaseChattingHolder(View v) {
        super(v);
    }
}