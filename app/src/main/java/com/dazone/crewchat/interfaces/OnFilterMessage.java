package com.dazone.crewchat.interfaces;

public interface OnFilterMessage {
    void onFilter(String text);
}