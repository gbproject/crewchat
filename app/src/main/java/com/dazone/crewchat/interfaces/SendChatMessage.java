package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ChattingDto;
import com.dazone.crewchat.dto.ErrorDto;

public interface SendChatMessage {
    void onSendChatMessageSuccess(ChattingDto chattingDto);
    void onSendChatMessageFail(ErrorDto errorDto, String url);
}
