package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.UnreadDto;

import java.util.List;

/**
 * Created by maidinh on 31-Aug-17.
 */

public interface UnreadCallBack {
    void onSuccess(List<UnreadDto> list);
    void onFail();
}
