package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ChatRoomDTO;
import com.dazone.crewchat.dto.ErrorDto;

import java.util.List;

public interface OnGetChatListV2 {
    void OnGetChatListSuccess(List<ChatRoomDTO> list);
    void OnGetChatListFail(ErrorDto errorDto);
}