package com.dazone.crewchat.interfaces;

public interface OnFinishCallback {
    void onFinish();
}