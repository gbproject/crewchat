package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.StatusDto;

public interface OnGetStatusCallback {
    void onGetStatusFinish();
}