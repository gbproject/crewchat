package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ErrorDto;
import com.dazone.crewchat.Tree.Dtos.TreeUserDTO;

import java.util.ArrayList;
import java.util.List;

public interface IGetListDepart {
    void onGetListDepartSuccess(ArrayList<TreeUserDTO> treeUserDTOs);
    void onGetListDepartFail(ErrorDto dto);
}
