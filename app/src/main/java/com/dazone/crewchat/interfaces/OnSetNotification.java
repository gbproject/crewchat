package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ErrorDto;

public interface OnSetNotification {
    void OnSuccess();
    void OnFail(ErrorDto errorDto);
}