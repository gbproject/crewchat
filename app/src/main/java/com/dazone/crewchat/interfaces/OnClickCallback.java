package com.dazone.crewchat.interfaces;

public interface OnClickCallback {
    void onClick();
}