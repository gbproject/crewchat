package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ChattingDto;
import com.dazone.crewchat.dto.ErrorDto;

/**
 * Created by THANHTUNG on 17/02/2016.
 */
public interface ICreateOneUserChatRom {
    void onICreateOneUserChatRomSuccess(ChattingDto chattingDto);
    void onICreateOneUserChatRomFail(ErrorDto errorDto);
}
