package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ErrorDto;
import com.dazone.crewchat.dto.userfavorites.FavoriteChatRoomDto;

import java.util.List;

public interface OnGetFavoriteChatRoom {
    void OnGetChatRoomSuccess(List<FavoriteChatRoomDto> list);
    void OnGetChatRoomFail(ErrorDto errorDto);
}