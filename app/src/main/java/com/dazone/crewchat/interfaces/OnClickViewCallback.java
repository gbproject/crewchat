package com.dazone.crewchat.interfaces;

public interface OnClickViewCallback {
    void onClick();
}