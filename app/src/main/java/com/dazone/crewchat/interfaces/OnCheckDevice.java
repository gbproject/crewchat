package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ErrorDto;

public interface OnCheckDevice {
    void onDeviceSuccess();
    void onHTTPFail(ErrorDto errorDto);
}