package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ChattingDto;
import com.dazone.crewchat.dto.ErrorDto;

import java.util.List;

public interface OnGetChatList {
    void OnGetChatListSuccess(List<ChattingDto> list);
    void OnGetChatListFail(ErrorDto errorDto);
}