package com.dazone.crewchat.interfaces;

import java.io.File;

/**
 * Created by maidinh on 26-Sep-17.
 */

public interface DownloadFileFromUrl {
    void onComplete(File file);
}
