package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ChatRoomDTO;
import com.dazone.crewchat.dto.ErrorDto;

public interface OnGetChatRoom {
    void OnGetChatRoomSuccess(ChatRoomDTO chatRoomDTO);
    void OnGetChatRoomFail(ErrorDto errorDto);
}