package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ErrorDto;
import com.dazone.crewchat.dto.ProfileUserDTO;

public interface OnGetUserCallBack {
    void onHTTPSuccess(ProfileUserDTO result);
    void onHTTPFail(ErrorDto errorDto);
}