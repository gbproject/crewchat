package com.dazone.crewchat.interfaces;

/**
 * Created by maidinh on 26-Sep-17.
 */

public interface AudioGetDuration {
    void onComplete(String duration);
}
