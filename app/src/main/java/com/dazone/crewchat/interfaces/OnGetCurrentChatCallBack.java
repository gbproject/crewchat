package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.CurrentChatDto;
import com.dazone.crewchat.dto.ErrorDto;

import java.util.List;

public interface OnGetCurrentChatCallBack {
    void onHTTPSuccess(List<CurrentChatDto> dtos);
    void onHTTPFail(ErrorDto errorDto);
}