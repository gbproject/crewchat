package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ChattingDto;
import com.dazone.crewchat.dto.ErrorDto;

import java.util.List;

public interface OnGetChatMessage {
    void OnGetChatMessageSuccess(List<ChattingDto> list);
    void OnGetChatMessageFail(ErrorDto errorDto);
}