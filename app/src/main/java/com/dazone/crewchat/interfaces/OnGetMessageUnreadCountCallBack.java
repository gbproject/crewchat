package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.ErrorDto;

public interface OnGetMessageUnreadCountCallBack {
    void onHTTPSuccess(String result);
    void onHTTPFail(ErrorDto errorDto);
}