package com.dazone.crewchat.interfaces;

import com.dazone.crewchat.dto.AttachImageList;

import java.util.List;

/**
 * Created by maidinh on 6/2/2017.
 */

public interface GetIvFileBox {
    void onSuccess(List<AttachImageList> lst);
    void onFail();
}
