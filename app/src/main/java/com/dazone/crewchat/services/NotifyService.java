package com.dazone.crewchat.services;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v4.app.JobIntentService;
import android.util.Log;

import com.dazone.crewchat.database.AllUserDBHelper;
import com.dazone.crewchat.database.BelongsToDBHelper;
import com.dazone.crewchat.database.ChatMessageDBHelper;
import com.dazone.crewchat.database.ChatRoomDBHelper;
import com.dazone.crewchat.database.DepartmentDBHelper;
import com.dazone.crewchat.database.FavoriteGroupDBHelper;
import com.dazone.crewchat.database.FavoriteUserDBHelper;
import com.dazone.crewchat.database.UserDBHelper;
import com.dazone.crewchat.eventbus.ReloadActivity;
import com.dazone.crewchat.gcm.GcmIntentService;
import com.dazone.crewchat.utils.CrewChatApplication;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by maidinh on 26-Nov-18.
 */

public class NotifyService extends JobIntentService {

    public static final int JOB_ID = 1;

    public static void enqueueWork(Context context, Intent work) {
        enqueueWork(context, NotifyService.class, JOB_ID, work);
    }

    @Override
    protected void onHandleWork(@NonNull Intent intent) {
        try {

            if (intent.getAction().compareTo(Intent.ACTION_LOCALE_CHANGED) == 0) {
                BelongsToDBHelper.clearBelong();
//                                    Log.d(TAG,"before delete AllUserDBHelper.getUser():"+AllUserDBHelper.getUser().size());
                AllUserDBHelper.clearUser();
//                                    Log.d(TAG,"after delete AllUserDBHelper.getUser():"+AllUserDBHelper.getUser().size());
                ChatRoomDBHelper.clearChatRooms();
                ChatMessageDBHelper.clearMessages();
                DepartmentDBHelper.clearDepartment();
                UserDBHelper.clearUser();
                FavoriteGroupDBHelper.clearGroups();
                FavoriteUserDBHelper.clearFavorites();
                // CrewChatApplication.getInstance().getPrefs().clear();
                CrewChatApplication.resetValue();
                EventBus.getDefault().post(new ReloadActivity());
                Log.d("LocaleChangedRecevier", "received ACTION_LOCALE_CHANGED");
            }
            // Explicitly specify that GcmIntentService will handle the intent.
            // ComponentName comp = new ComponentName(context.getPackageName(), GcmIntentService.class.getName());
            // Start the service, keeping the device awake while it is launching.
            // startWakefulService(context, (intent.setComponent(comp)));
            //setResultCode(Activity.RESULT_OK);

        }catch (Exception e){
            e.printStackTrace();
        }
    }


}