package com.dazone.crewchat.TestMultiLevelListview;

import com.dazone.crewchat.Tree.Dtos.TreeUserDTO;

public interface NLevelListItem {
	TreeUserDTO getObject();
	boolean isExpanded();
	void toggle();
	NLevelListItem getParent();
	int getLevel();
	// public View getView();
}
