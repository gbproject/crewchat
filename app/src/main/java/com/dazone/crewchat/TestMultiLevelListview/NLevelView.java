package com.dazone.crewchat.TestMultiLevelListview;

import android.view.View;

public interface NLevelView {

	View getView(NLevelItem item);
}
