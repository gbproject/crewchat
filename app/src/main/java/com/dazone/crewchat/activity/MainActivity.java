package com.dazone.crewchat.activity;

import android.Manifest;
import android.app.ActivityManager;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Toast;

import com.dazone.crewchat.BuildConfig;
import com.dazone.crewchat.HTTPs.GetUserStatus;
import com.dazone.crewchat.HTTPs.HttpRequest;
import com.dazone.crewchat.R;
import com.dazone.crewchat.TestMultiLevelListview.MultilLevelListviewFragment;
import com.dazone.crewchat.activity.base.BasePagerActivity;
import com.dazone.crewchat.adapter.TabPagerAdapter;
import com.dazone.crewchat.constant.Statics;
import com.dazone.crewchat.database.AllUserDBHelper;
import com.dazone.crewchat.database.UserDBHelper;
import com.dazone.crewchat.dto.ErrorDto;
import com.dazone.crewchat.dto.StatusDto;
import com.dazone.crewchat.dto.StatusItemDto;
import com.dazone.crewchat.dto.TreeUserDTOTemp;
import com.dazone.crewchat.dto.UserInfoDto;
import com.dazone.crewchat.eventbus.NotifyAdapterOgr;
import com.dazone.crewchat.eventbus.ReloadActivity;
import com.dazone.crewchat.fragment.BaseFavoriteFragment;
import com.dazone.crewchat.fragment.CompanyFragment;
import com.dazone.crewchat.fragment.CurrentChatListFragment;
import com.dazone.crewchat.interfaces.OnClickCallback;
import com.dazone.crewchat.interfaces.OnGetStatusCallback;
import com.dazone.crewchat.interfaces.OnGetUserInfo;
import com.dazone.crewchat.services.SyncStatusService;
import com.dazone.crewchat.utils.Constant;
import com.dazone.crewchat.utils.CrewChatApplication;
import com.dazone.crewchat.utils.Prefs;
import com.dazone.crewchat.utils.Utils;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static com.dazone.crewchat.ViewHolders.ListCurrentViewHolder.roomNo1;

public class MainActivity extends BasePagerActivity implements ViewPager.OnPageChangeListener, ServiceConnection {
    String TAG = "MainActivity";
    private boolean doubleBackToExitPressedOnce = false;
    public static MainActivity instance = null;
    public static long myRoom = -55;
    public static String type;
    protected Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            boolean aResponse = msg.getData().getBoolean("is_update");

            if (aResponse) {
                int tab = msg.getData().getInt("tab");

//                if (tab == TAB_COMPANY) {
//                    ArrayList<TreeUserDTOTemp> lst = AllUserDBHelper.getUser();
//
//                } else if (tab == TAB_FAVORITE) {
////                    AllUserDBHelper.getUser()
//
//                }

                if (mGetStatusCallbackCompany != null && tab == TAB_COMPANY) {
                    mGetStatusCallbackCompany.onGetStatusFinish();
                    Log.d(TAG, "mGetStatusCallbackCompany");

                }

                if (mGetStatusCallbackFavorite != null && tab == TAB_FAVORITE) {
                    mGetStatusCallbackFavorite.onGetStatusFinish();
                }
            }
        }
    };

    static boolean active = false;

    public static int TAB_CHAT = 0;
    public static int TAB_COMPANY = 1;
    public static int TAB_FAVORITE = 2;
    public static int TAB_SETTING = 3;
    public static Uri imageUri;
    private int currentUserNo = 0;

    private SyncStatusService syncStatusService = null;
    private boolean isBound = false;
    private int companyNo;
    public static ArrayList<String> mSelectedImage = new ArrayList<>();

    private OnGetStatusCallback mGetStatusCallbackCompany, mGetStatusCallbackFavorite;

    public void setGetStatusCallbackCompany(OnGetStatusCallback mGetStatusCallback) {
        this.mGetStatusCallbackCompany = mGetStatusCallback;
    }

    public void setGetStatusCallbackFavorite(OnGetStatusCallback mGetStatusCallback) {
        this.mGetStatusCallbackFavorite = mGetStatusCallback;
    }


    @Override
    protected void init() {


//        Log.d(TAG,"getRegDate"+ TimeUtils.displayTimeWithoutOffset(CrewChatApplication.getInstance().getApplicationContext(),"/Date(1490835539991+0900)/", 0, TimeUtils.KEY_FROM_SERVER));
//        Log.d(TAG, "getRegDate"+TimeUtils.displayTimeWithoutOffset(CrewChatApplication.getInstance().getApplicationContext(), "/Date(1490831939991+0900)/", 0, TimeUtils.KEY_FROM_SERVER));


        File dir = new File(Environment.getExternalStorageDirectory() + Constant.pathDownload_no);
        if (dir.exists() && dir.isDirectory()) {
            // do something here
            Log.d(TAG, "dir exists");
        } else {
            dir.mkdirs();
            Log.d(TAG, "dir dont exists");
        }

        instance = this;
        myRoom = Statics.MYROOM_DEFAULT;
        CURRENT_TAB = 0;
        currentUserNo = Utils.getCurrentId();
        companyNo = new Prefs().getCompanyNo();

        active = true;
        getSupportActionBar().setDisplayHomeAsUpEnabled(false);


        // Load client data after login
        if (!CrewChatApplication.isLoggedIn) {
            Log.d(TAG, "loadStaticLocalData");
            CrewChatApplication.getInstance().syncData();
            CrewChatApplication.getInstance().loadStaticLocalData();
            CrewChatApplication.isLoggedIn = true;
            CrewChatApplication.currentId = currentUserNo;

        }

        //TODO SON ADD SHARE FUNCTION


      /*  if (prefs.getLongValue(String.valueOf(roomNo1), 0) > 0) {
            Log.d("sssintent", "" + (prefs.getLongValue(String.valueOf(roomNo1), 0)));
            Intent intent = new Intent(this, ChattingActivity.class);
            intent.putExtra(Constant.KEY_INTENT_ROOM_NO, prefs.getLongValue(String.valueOf(roomNo1), 0));
            startActivity(intent);
        }*/
        // Show FAB button at the first time
        // showPAB(mAddUserCallback);
    }
    public void setPermissionsReadExternalStorage() {
        String[] requestPermission;
        requestPermission = new String[]{Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE,};
        ActivityCompat.requestPermissions(this, requestPermission, 0);
    }
    @Override
    protected void inItShare() {
        if (Utils.checkStringValue(prefs.getaccesstoken()) && !prefs.getBooleanValue(Statics.PREFS_KEY_SESSION_ERROR, false)) {
            final Intent intent = getIntent();
            final String action = intent.getAction();
            type = intent.getType();
            if (type != null) {
                setPermissionsReadExternalStorage();
                tabAdapter = new TabPagerAdapter(getSupportFragmentManager(), 2, this);
                mViewPager.setAdapter(tabAdapter);
                mViewPager.setOffscreenPageLimit(1);
                tabLayout.setupWithViewPager(mViewPager);
                setupTabTwo();
                setupViewPager();

            } else {
                tabAdapter = new TabPagerAdapter(getSupportFragmentManager(), 4, this);
                mViewPager.setAdapter(tabAdapter);
                mViewPager.setOffscreenPageLimit(3);
                tabLayout.setupWithViewPager(mViewPager);
                setupTab();
                setupViewPager();
            }
            mHandler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    if (Intent.ACTION_SEND.equals(action) && type != null) {
                        if ("text/plain".equals(type)) {
                            // handleSendText(intent);
                        } else if (type.startsWith("video/")) {
                            handleSendVideo(intent);
                        } else if (type.startsWith("audio/")) {
                            handleSendAudio(intent);
                        } else if (type.startsWith("text/")) {
                            handleSendContact(intent);
                        } else {
                            handleSendFile(intent);
                        }
                    } else if (Intent.ACTION_SEND_MULTIPLE.equals(action) && type != null) {
                        handleSendMultipleFile(intent);
                    } else {
                        // Handle other intents, such as being started from the home screen
                    }
                }
            }, 1500);
        } else {
            Intent intent = new Intent(this, LoginActivity.class);
            //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
        }
    }

    void handleSendContact(Intent intent) {
        imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            // Update UI to reflect image being shared
            //GetFile(imageUri);
            //  fab.performClick();

        }
    }

    void handleSendImage(Intent intent) {
        imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        mSelectedImage.clear();
        if (imageUri != null) {
            mSelectedImage.add(Utils.getPathFromURI(imageUri, getApplicationContext()));
            // Update UI to reflect image being shared
            //GetFile(imageUri);
            //fab.performClick();

        }
    }

    void handleSendVideo(Intent intent) {
        imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            //  fab.performClick();
        }
    }

    void handleSendAudio(Intent intent) {
        imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            //fab.performClick();
        }
    }

    void handleSendFile(Intent intent) {
        imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        mSelectedImage.clear();
        if (imageUri != null) {
            if (mSelectedImage != null) {
                mSelectedImage.add(Utils.getPathFromURI(imageUri, getApplicationContext()));
            }
            //   fab.performClick();
       /*   Intent myIntent = new Intent(this, ShareActivity.class);
            //   myIntent.putExtra("key", value); //Optional parameters
            this.startActivity(myIntent);*/

        }
    }


    void handleSendMultipleImages(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        mSelectedImage.clear();
        if (imageUris != null) {
            // Update UI to reflect multiple images being shared
            for (Uri path : imageUris) {
                // Do something with the URI
                //   GetFile(path);
                if (mSelectedImage != null) {
                    mSelectedImage.add(Utils.getPathFromURI(path, getApplicationContext()));
                }

            }
            //fab.performClick();
        }
    }

    void handleSendMultipleFile(Intent intent) {
        ArrayList<Uri> imageUris = intent.getParcelableArrayListExtra(Intent.EXTRA_STREAM);
        mSelectedImage.clear();
        if (imageUris != null) {
            // Update UI to reflect multiple images being shared
            for (Uri path : imageUris) {
                // Do something with the URI
                //   GetFile(path);
                if (mSelectedImage != null) {
                    mSelectedImage.add(Utils.getPathFromURI(path, getApplicationContext()));
                }

            }
            fab.performClick();
        }

    }

    private void goOrganization() {
        if (MainActivity.CURRENT_TAB == 0) {
            if (MainActivity.instance != null) {
                MainActivity.instance.gotoOrganizationChart();
            }
        } else if (MainActivity.CURRENT_TAB == 2) {
            if (BaseFavoriteFragment.CURRENT_TAB == 0) {
//                        MainActivity.instance.gotoOrganizationChart();
            } else {
                if (MultilLevelListviewFragment.instanceNew != null
                        && MultilLevelListviewFragment.instanceNew.isLoadDB()) {
                    MultilLevelListviewFragment.instanceNew.addFavorite();
                }
            }


        }
    }

    public int currentItem() {
        return mViewPager.getCurrentItem();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if(new Prefs().getIntValue("PAGE", 0)==TAB_COMPANY){
            //EventBus.getDefault().post(new NotifyAdapterOgr());
        }
        mViewPager.setCurrentItem(new Prefs().getIntValue("PAGE", 0));

    }

    @Override
    protected void onStart() {
        super.onStart();
        // Bind service to sync status data
        Intent objIntent = new Intent(this, SyncStatusService.class);
        bindService(objIntent, this, Context.BIND_AUTO_CREATE);
        // Get intent, action and MIME type

    }

    protected void setupViewPager() {
        mViewPager.addOnPageChangeListener(this);
    }

    protected void setupTab() {
        if (tabLayout == null) {
            return;
        }

        View view = LayoutInflater.from(this).inflate(R.layout.custom_tab_view, null);
        tabLayout.getTabAt(0).setCustomView(view);
        tabLayout.getTabAt(1).setIcon(R.drawable.tabbar_group_ic);
        tabLayout.getTabAt(2).setIcon(R.drawable.nav_favorite_ic);
        tabLayout.getTabAt(3).setIcon(R.drawable.nav_mnu_hol_ic);

//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (CurrentChatListFragment.fragment != null) {
//                    CurrentChatListFragment.fragment.searchAction(0);
//                }
//            }
//        });

    }

    protected void setupTabTwo() {
        if (tabLayout == null) {
            return;
        }

        View view = LayoutInflater.from(this).inflate(R.layout.custom_tab_view, null);
        tabLayout.getTabAt(0).setCustomView(view);
        tabLayout.getTabAt(1).setIcon(R.drawable.tabbar_group_ic);
     /*   tabLayout.getTabAt(2).setIcon(R.drawable.nav_favorite_ic);
        tabLayout.getTabAt(3).setIcon(R.drawable.nav_mnu_hol_ic);*/

//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                if (CurrentChatListFragment.fragment != null) {
//                    CurrentChatListFragment.fragment.searchAction(0);
//                }
//            }
//        });

    }

    @Override
    public void onBackPressed() {
        if (doubleBackToExitPressedOnce) {
            super.onBackPressed();
            return;
        }

        this.doubleBackToExitPressedOnce = true;

        Toast.makeText(this, getResources().getString(R.string.press_again_to_exit_message), Toast.LENGTH_SHORT).show();
        mHandler.postDelayed(new Runnable() {
            @Override
            public void run() {
                doubleBackToExitPressedOnce = false;
            }
        }, 2000);
    }

    private int currentPage;

    @Subscribe
    public void reloadActivity(ReloadActivity reloadActivity) {
        finish();
        ActivityManager activityManager = (ActivityManager) this.getSystemService(ACTIVITY_SERVICE);
        List<ActivityManager.RunningAppProcessInfo> procInfos = activityManager.getRunningAppProcesses();
        for (int i = 0; i < procInfos.size(); i++) {
            if (procInfos.get(i).processName.equals(BuildConfig.APPLICATION_ID)) {
                startActivity(getIntent());
            }
        }
    }

    public int getCurrentPage() {
        return currentPage;
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    public void showMenuSearch(OnClickCallback callback) {
        if (menuItemSearch != null) {
            menuItemSearch.setVisible(true);
            searchView.setVisibility(View.VISIBLE);
        }
    }

    public void hideMenuSearch() {
        Log.d(TAG, "hideMenuSearch");
        if (menuItemSearch != null) {
            searchView.setIconified(true);
            searchView.setVisibility(View.GONE);
            menuItemMore.setVisible(false);
            menuItemSearch.collapseActionView();
            menuItemSearch.setVisible(false);
        }
    }

    public void gotoOrganizationChart() {
        boolean flag = new Prefs().isDataComplete();
        if (flag && CompanyFragment.instance != null
                && CompanyFragment.instance.getSubordinates().size() > 0
                && CompanyFragment.instance.isLoadTreeData()) {
            Intent intent = new Intent(MainActivity.this, NewOrganizationChart.class);
            intent.putExtra(Statics.IS_NEW_CHAT, true);
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), "wait get list user finish", Toast.LENGTH_SHORT).show();
        }


    }


    // 탭을 직접 선택 했을 경우 이벤트 처리
    public static int CURRENT_TAB = 0;


    @Override
    public void onPageSelected(final int position) {

        if (position != TAB_CHAT) {
            if (CurrentChatListFragment.fragment != null)
                CurrentChatListFragment.fragment.justHide();
        }


        MainActivity.CURRENT_TAB = position;
        currentPage = position;
        new Prefs().putIntValue("POSITION_PAGE", position);

//        if (position == TAB_CHAT) {
//            if (CurrentChatListFragment.fragment != null) {
//                CurrentChatListFragment.fragment.updateStatus();
//            }
//        }

        if (position == TAB_CHAT || position == TAB_FAVORITE) {
            showPAB();
            hideSearchView();
            hideMenuSearch();
            showIcon();
            if (position == TAB_FAVORITE) {
//                Log.d(TAG,"BaseFavoriteFragment.CURRENT_TAB:"+BaseFavoriteFragment.CURRENT_TAB);
                if (BaseFavoriteFragment.CURRENT_TAB == 0) {
                    hidePAB();
                } else {

                }
            }
        } else if (position == TAB_COMPANY ) {
            hideSearchIcon();
            hidePAB();
        }else if (position == TAB_SETTING) {
            hideSearchIcon();
            hidePAB();
            hideSearchView();
        }
        if (position == TAB_FAVORITE || position == TAB_COMPANY) {
            if (position == TAB_COMPANY || position == TAB_FAVORITE) {
                final ArrayList<TreeUserDTOTemp> users = AllUserDBHelper.getUser();
                boolean isStaticList = false;
                if (CrewChatApplication.listUsers != null && CrewChatApplication.listUsers.size() > 0) {
                    isStaticList = true;
                }
                final boolean finalIsStaticList = isStaticList;
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        final StatusDto status = new GetUserStatus().getStatusOfUsers(new Prefs().getHOST_STATUS(), companyNo);

                        if (status != null) {

                            for (final TreeUserDTOTemp u : users) {
                                boolean isUpdate = false;

                                for (final StatusItemDto sItem : status.getItems()) {

                                    if (sItem.getUserID().equals(u.getUserID())) {
//                                    Log.d(TAG, "updateStatus 3");
                                        Log.d(TAG, "updateStatus 3:" + u.getName() + " # " + u.getDBId() + " # " + sItem.getUserID() + " # " + sItem.getStatus());
                                        AllUserDBHelper.updateStatus(u.getDBId(), sItem.getStatus());

                                        if (finalIsStaticList) {
                                            boolean xUpdate = false;
                                            TreeUserDTOTemp temp = null;

                                            for (TreeUserDTOTemp uu : CrewChatApplication.listUsers) {
                                                temp = uu;

                                                if (sItem.getUserID().equals(uu.getUserID())) {
                                                    uu.setStatus(sItem.getStatus());
                                                    xUpdate = true;
                                                    break;
                                                }
                                            }

                                            if (!xUpdate) {
                                                if (temp != null) {
                                                    temp.setStatus(Statics.USER_LOGOUT);
                                                }
                                            }
                                        }

                                        isUpdate = true;
                                        break;
                                    }
                                }

                                if (!isUpdate) {
                                    if (u.getUserNo() == currentUserNo) {
                                        new Thread(new Runnable() {
                                            @Override
                                            public void run() {
                                                // 내 상태값을 바꾼다.
                                                UserDBHelper.updateStatus(u.getUserNo(), Statics.USER_LOGOUT);
                                            }
                                        }).start();
                                    }
                                    new Thread(new Runnable() {
                                        @Override
                                        public void run() {
                                            // 나 이외의 사용자 상태값을 바꾼다.
//                                        Log.d(TAG, "updateStatus 4");
                                            AllUserDBHelper.updateStatus(u.getDBId(), Statics.USER_LOGOUT);
                                        }
                                    }).start();
                                }
                            }

                            // Send message to main thread after update status
                            Message msgObj = mHandler.obtainMessage();
                            Bundle b = new Bundle();
                            b.putBoolean("is_update", true);
                            if (position == TAB_COMPANY) {
                                b.putInt("tab", TAB_COMPANY);
                            } else {
                                b.putInt("tab", TAB_FAVORITE);
                            }

                            msgObj.setData(b);
                            Log.d(TAG, "mHandler 1");
                            mHandler.sendMessage(msgObj);
                        }

                    }
                }).start();

                // 유저 상태메시지 처리
                HttpRequest.getInstance().getAllUserInfo(new OnGetUserInfo() {
                    @Override
                    public void OnSuccess(final ArrayList<UserInfoDto> userInfo) {
                        new Thread(new Runnable() {
                            @Override
                            public void run() {

                                // If list user is #null then update user status string
                                if (users == null) {
                                    ArrayList<TreeUserDTOTemp> tempUsers = AllUserDBHelper.getUser();
                                    for (TreeUserDTOTemp sItem : tempUsers) {
                                        for (UserInfoDto u : userInfo) {
                                            if (sItem.getUserNo() == u.getUserNo()) {
                                                AllUserDBHelper.updateStatusString(sItem.getDBId(), u.getStateMessage());
                                            }
                                        }
                                    }
                                } else {
                                    for (TreeUserDTOTemp sItem : users) {
                                        for (UserInfoDto u : userInfo) {
                                            if (sItem.getUserNo() == u.getUserNo()) {
                                                AllUserDBHelper.updateStatusString(sItem.getDBId(), u.getStateMessage());
                                                sItem.setUserStatusString(u.getStateMessage());
                                            }
                                        }

                                    }
                                }

                                // Send message to main thread after update status
                                Message msgObj = mHandler.obtainMessage();
                                Bundle b = new Bundle();
                                b.putBoolean("is_update", true);
                                if (position == TAB_COMPANY) {
                                    b.putInt("tab", TAB_COMPANY);
                                } else {
                                    b.putInt("tab", TAB_FAVORITE);
                                }

                                msgObj.setData(b);
//                            Log.d(TAG,"mHandler 2");
                                mHandler.sendMessage(msgObj);

                            }
                        }).start();
                    }

                    @Override
                    public void OnFail(ErrorDto errorDto) {
                    }
                });
            }
        }

        Utils.hideKeyboard(this);
        currentPage = position;
        Fragment page = getSupportFragmentManager().findFragmentByTag("android:switcher:" + R.id.container + ":" + mViewPager.getCurrentItem());
        new Prefs().putIntValue("PAGE", position);

        // based on the current position you can then cast the page to the correct
        // class and call the method:
        // 탭 화면이 스크롤 되어 질 때에 이벤트 처리(좌우), 화면에 보이는 검색 아이콘등을 설정

        if (position != TAB_COMPANY) {
            hideMenuSearch();
        } else {
            if (menuItemSearch != null && searchView != null) {
                menuItemSearch.setVisible(true);
                searchView.setVisibility(View.VISIBLE);
            } else {
                Log.d(TAG, "menuItemSearch null");
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        if (menuItemSearch != null && searchView != null) {
                            menuItemSearch.setVisible(true);
                            searchView.setVisibility(View.VISIBLE);
                        } else {
                            Log.d(TAG, "still null");
                        }
                    }
                }, 2000);
            }
        }

        if (mViewPager.getCurrentItem() == TAB_CHAT && page != null) {
            if (menuItemSearch != null) {
                searchView.setIconified(true);
                searchView.setVisibility(View.GONE);
                menuItemMore.setVisible(false);
                menuItemSearch.collapseActionView();
                menuItemSearch.setVisible(false);
            }
        } else if (mViewPager.getCurrentItem() == TAB_COMPANY && page != null) {
        } else if (mViewPager.getCurrentItem() == TAB_FAVORITE && page != null) {
            if (menuItemSearch != null && menuItemMore != null) {
                menuItemSearch.collapseActionView();
                menuItemSearch.setVisible(false);
            }
        } else {
            if (menuItemSearch != null && menuItemMore != null) {
                searchView.setIconified(true);
                searchView.setVisibility(View.GONE);
                menuItemMore.setVisible(false);
                menuItemSearch.collapseActionView();
                menuItemSearch.setVisible(false);
            }
        }
    }

    @Override
    public void onPageScrollStateChanged(int state) {
    }

    public static void cancelAllNotification(Context ctx) {
        String ns = Context.NOTIFICATION_SERVICE;
        NotificationManager nMgr = (NotificationManager) ctx.getSystemService(ns);
        nMgr.cancelAll();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy");
        myRoom = Statics.MYROOM_DEFAULT;
        cancelAllNotification(CrewChatApplication.getInstance());

        if (isBound) {
            unbindService(this);
        }

        active = false;
    }

    @Override
    public void onServiceConnected(ComponentName name, IBinder service) {
        SyncStatusService.Binder binder = (SyncStatusService.Binder) service;
        syncStatusService = binder.getMyService();

        new Thread(new Runnable() {
            @Override
            public void run() {
                syncStatusService.syncData();
                syncStatusService.syncStatusString();
            }
        }).start();

        isBound = true;
    }

    @Override
    public void onServiceDisconnected(ComponentName name) {
        isBound = false;
    }
}