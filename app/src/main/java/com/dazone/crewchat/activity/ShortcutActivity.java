package com.dazone.crewchat.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.dazone.crewchat.R;
import com.dazone.crewchat.activity.base.BaseActivity;
import com.dazone.crewchat.constant.Statics;
import com.dazone.crewchat.dto.ChattingDto;
import com.dazone.crewchat.utils.Constant;

import static com.dazone.crewchat.ViewHolders.ListCurrentViewHolder.CHILD_NAME_DOB;
import static com.dazone.crewchat.ViewHolders.ListCurrentViewHolder.roomNo1;

public class ShortcutActivity extends BaseActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shortcut);
        Intent intent1 = getIntent();
        //Log.d("sssintent", intent1.toString());
      //  Log.d("sssintent", "" + intent1.getStringExtra(Intent.EXTRA_SHORTCUT_NAME));
       // if (CHILD_NAME_DOB.containsKey(intent1.getStringExtra(Intent.EXTRA_SHORTCUT_NAME))) {
         //   long roomValue = Long.parseLong(CHILD_NAME_DOB.get(intent1.getStringExtra(Intent.EXTRA_SHORTCUT_NAME)));
            //    Log.d("sssintent", "" + roomValue);
            //   Log.d("sssintent", "" + intent1.getStringExtra(Intent.EXTRA_SHORTCUT_NAME));
            Intent intent = new Intent(this, ChattingActivity.class);
            intent.putExtra(Constant.KEY_INTENT_ROOM_NO, roomNo1);
            startActivity(intent);
        //}


    }



}
